/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Estudiante;

/**
 *  Clase del negocio para la manipulación de una lista de Calificaciones
 * @author Marco Adarme
 */
public class SistemaCalificaciones {
    
    private Estudiante []listaEstructuras;

    public SistemaCalificaciones() {
    }
    
    
     public SistemaCalificaciones(int can) {
         
         // T identi[] = new T[tamaño];
         //T es Estudiante
            this.listaEstructuras=new Estudiante[can];
    }
    
    
     //Esto va a cambiar
     public void insertarEstudiante_EnPos0(long codigo,String nombre,  String notas)
     {
         Estudiante nuevo=new Estudiante(codigo, nombre, notas);
         
         this.listaEstructuras[0]=nuevo;
         
     }

    @Override
    public String toString() {
        
        String msg="Mis estudiantes son:\n";
        
        for(Estudiante unEstudiante:this.listaEstructuras)
            msg+=unEstudiante.toString();
        
        return msg;
        
    }
     
     
     
     
    
}
